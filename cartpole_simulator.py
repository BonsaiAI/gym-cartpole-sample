import gym

import bonsai
from bonsai_gym_common import GymSimulator

ENVIRONMENT = 'CartPole-v0'
RECORD_PATH = None


class CartPoleSimulator(GymSimulator):

    def __init__(self, env, record_path, render_env):
        GymSimulator.__init__(
            self, env, skip_frame=1,
            record_path=record_path, render_env=render_env)

    def get_state(self):
        parent_state = GymSimulator.get_state(self)
        state_dict = {"position": parent_state.state[0],
                      "velocity": parent_state.state[1],
                      "angle": parent_state.state[2],
                      "rotation": parent_state.state[3]}
        return bonsai.simulator.SimState(state_dict, parent_state.is_terminal)


if __name__ == "__main__":
    env = gym.make(ENVIRONMENT)
    base_args = bonsai.parse_base_arguments()
    simulator = CartPoleSimulator(env, RECORD_PATH, not base_args.headless)
    bonsai.run_with_url("cartpole_simulator", simulator, base_args.brain_url)
